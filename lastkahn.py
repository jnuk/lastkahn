#!/usr/bin/env python3
# imports
import sys
import numpy as np
from pprint import pprint
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
from matplotlib.patches import PathPatch
from matplotlib.animation import FuncAnimation
import matplotlib.transforms as transforms
import pygame
import time
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import json

# check for valid call
if len(sys.argv) != 3:
	sys.exit("Usage: ./lastkahn.py <ship-config.json> <stream-data.txt>")

# Constants
rho = 1e3	# Dichte von Wasser: 1000 kg/m^3
nf = 1		# Punkte an Bug und Heck
ns = 2		# Punkte an Seite (jeweils)

# Classes
class Stroemung:
	def __init__(self, vektorfeld = None):
		if vektorfeld is not None:
			self.x = list(x[0] for x in vektorfeld)
			self.y = list(y[1] for y in vektorfeld)
			self.u = list(u[2] for u in vektorfeld)
			self.v = list(v[3] for v in vektorfeld)
			self.begrenzungLinks = list(l[4] for l in vektorfeld)
			self.begrenzungRechts = list(r[5] for r in vektorfeld)
		else:
			self.x = list()
			self.y = list()
			self.u = list()
			self.v = list()
			self.begrenzungLinks = list()
			self.begrenzungRechts = list()

	def addVektor(self, vektor):
		self.x += [vektor[0]]
		self.y += [vektor[1]]
		self.u += [vektor[2]]
		self.v += [vektor[3]]

	def addBegrenzungsPunkt(self, left, punkt):
		if left:
			self.begrenzungLinks.append(punkt)
		else:
			self.begrenzungRechts.append(punkt)

	def finalizeBegrenzungen(self, ax):
		left, right = ax.get_xlim()
		bottom, top = ax.get_ylim()
		self.begrenzungLinks.append([left, bottom])
		self.begrenzungRechts.append([right, top])


class Ship:
	def __init__(self, c0, c2, a, l, b, h, m, I, cw_f, cw_s, a_max, v_max, w_maxRudder):
		self.corners = np.zeros((5,2))
		# Ort des Eckpunktes links unten [x, y]
		self.corners[0] = np.array(c0)
		# Ort des Eckpunktes rechts oben [x, y]
		self.corners[2] = np.array(c2)
		#Masse des Schiffes in kg
		self.m = m
		# Länge
		self.l = l
		# Breite			
		self.b = b
		# Höhe
		self.h = h
		# Maximale Beschleunigung
		self.a_max = a_max
		# Maximale Geschwindigkeit
		self.v_max = v_max
		# Geschwindigkeitsvektor [v_x, v_y]
		self.velocity = np.array([0,0])
		# Strömungswiderstandskoeffizient frontal
		self.cw_f = cw_f
		# Strömungswiderstandskoeffizient seitlich
		self.cw_s = cw_s
		# Drehwinkel zur y-Achse in Grad (gegen Uhrzeigersinn -> positiv) BOGENMASS
		self.angle = a
		# Schub
		self.thrust = 1.0
		# Ruderlage
		self.rudder = 0.
		# Maximales Ruder-Winkelgeschw.
		self.w_maxRudder = w_maxRudder
		# Winkelgeschwindigkeit
		self.w = 0.
		# Drehmoment
		self.M = 0.
		# Auf Schwerpunkt wirkende Kraft			
		self.F = np.array([0,0])
		# Trägheitsmoment
		self.I = I
		# Berechne andere Eckpunkte
		self.calcCorners()
		
	def iterate(self, time):	 #time = Zeitintervall
		# vor Aufruf F und M updaten!

		# Captain Beschleunigung als Vektor
		ACV = np.array([-self.Acaptain() * np.sin(self.angle), self.Acaptain() * np.cos(self.angle)])

		# Translation
		self.corners[0] = self.corners[0] + (0.5 * ((self.F / self.m) + ACV) * time**2 + self.velocity * time)
		self.corners[2] = self.corners[2] + (0.5 * ((self.F / self.m) + ACV) * time**2 + self.velocity * time)

		self.velocity = self.velocity + ((self.F / self.m + ACV)* time)

		# Rotation
		self.w = self.w + (self.M / self.I * time)
		dangle = (0.5 * (self.M / self.I) * time**2 + (self.w+self.w_rudder()) * time)
		self.angle = self.angle + dangle

		R = np.matrix([[np.cos(dangle), -np.sin(dangle)], [np.sin(dangle), np.cos(dangle)]])

		center = self.getCenter()
		self.corners[0] = np.array(R).dot(self.corners[0] - center) + center
		self.corners[2] = np.array(R).dot(self.corners[2] - center) + center

		# Calculate other corners
		self.calcCorners()
		
	def calcCorners(self): #Format: Liste mit corners(Listen) als Element
		self.corners[1] = np.array([self.corners[0][0] - self.l*np.sin(self.angle),
									self.corners[0][1] + self.l*np.cos(self.angle)])
		self.corners[3] = np.array([self.corners[0][0] + self.b*np.cos(self.angle),
									self.corners[0][1] + self.b*np.sin(self.angle)])

	def getCornerPoints(self):
		return [Point(self.corners[0]),
				Point(self.corners[1]),
				Point(self.corners[2]),
				Point(self.corners[3])]

	def getCenter(self):
		return self.corners[0] + (1/2 * (self.corners[2] - self.corners[0]))

	def update(self, stroemung):
		self.F = Physics.F_Rges(self, stroemung, nf, ns)
		self.M = Physics.M_ges(self, stroemung, nf, ns)
		self.iterate(1)

	def increaseThrust(self):
		if self.thrust < 1:
			self.thrust += 0.01
	def decreaseThrust(self):
		if self.thrust > 0:
			self.thrust -= 0.01

	def Acaptain(self):
		return Physics.accel(self, self.thrust)

	def steerLeft(self):
		if self.rudder < 1:
			self.rudder += 0.01

	def steerRight(self):
		if self.rudder > -1:
			self.rudder -= 0.01

	def w_rudder(self):
		return Physics.angVel(self.w_maxRudder, self.rudder, self.velocity, self.v_max)


class Physics:
	def F_R(n,c_w,l,h,v_Boot,v_Wasser,oberflvek): # v_Boot, v_Wasser, oberflvek sind numpy-Vektoren! ls=true => Längsseite, ls=false => Breitseite
		# Relativgeschwindigkeit
		v_rel = v_Boot - v_Wasser

		if v_rel[0] == 0 and v_rel[1] == 0:
			return np.array([0,0])

		winkelv_relOberflvek = np.arccos(np.vdot(v_rel, oberflvek)/(np.linalg.norm(v_rel)*np.linalg.norm(oberflvek)))
		
		normF = rho*(1/(4*n))*c_w*l*h*((np.linalg.norm(v_rel))**2)*np.sin(winkelv_relOberflvek)

		vector = normF * v_rel / np.linalg.norm(v_rel)

		a = np.pi/2 - winkelv_relOberflvek
		R1 = np.matrix([[np.cos(a), -np.sin(a)], [np.sin(a), np.cos(a)]])
		R2 = np.matrix([[np.cos(2*np.pi-a), -np.sin(2*np.pi-a)], [np.sin(2*np.pi-a), np.cos(2*np.pi-a)]])

		moeglichkeit1 = np.vdot(np.array(R1).dot(vector), oberflvek)
		moeglichkeit2 = np.vdot(np.array(R2).dot(vector), oberflvek)

		if abs(moeglichkeit1) <= abs(moeglichkeit2):
			return -np.array(R1).dot(vector)
		else:
			return -np.array(R2).dot(vector)

	def makeOberflvek(schiff, l): # schiff ist ein Ship-Objekt, l=true => Längsseite, l=false => Breitseite
		if l:
			return np.array([np.array(schiff.corners[1])-np.array(schiff.corners[0])])
		else:
			return np.array([np.array(schiff.corners[3])-np.array(schiff.corners[0])])

	def I(m,l,b):
		return (1/12) * m * (l**2 + b**2)

	def M_i(F_R,r):
		return np.cross(r, F_R)

	def closestVector(p, stroemung):
		dmin = 9.99e63
		umin = 0
		vmin = 0
		for x, y, u, v in zip(stroemung.x, stroemung.y, stroemung.u, stroemung.v):
			if abs(x-p[0]) <= 400 and abs(y-p[1]) <= 400:
				d = (x-p[0])**2 + (y-p[1])**2
				if d < dmin:
					dmin = d
					umin = u
					vmin = v
		return np.array([umin, vmin])

	def calcM(m_max, load):
		return (m_max/10)+load*(m_max-m_max/10)

	def calcH(h_max,m_max,mass,l,b):
		return h_max - ((m_max - mass)/(rho*l*b))

	def a_max(v_max, h, b, m):
		return 0.25 * rho * h * b * (v_max-3)**2 / m

	def accel(schiff, thrust):
		return schiff.a_max*(1-np.e**(-4*thrust))

	def angVel(w_maxRudder, rudder, velocity, v_max):
		return w_maxRudder * np.sin(np.pi/2 * rudder) * np.linalg.norm(velocity) / v_max

	def F_Rges(schiff,stroemung,nf,ns):
		F_res = np.array([0.0,0.0])

		# Bug
		for i in range(1,nf+1):
			p = schiff.corners[1] + (i * (1/(nf+1))*(schiff.corners[2] - schiff.corners[1]))

			vektor = Physics.F_R(nf, schiff.cw_f, schiff.b, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, False))
			F_res = F_res + vektor

		# Heck
		for i in range(1,nf+1):
			p = schiff.corners[0] + (i * (1/(nf+1))*(schiff.corners[3] - schiff.corners[0]))

			vektor = Physics.F_R(nf, schiff.cw_f, schiff.b, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, False))
			F_res = F_res + vektor

		# Steuerbordseite
		for i in range(1,ns+1):
			p = schiff.corners[3] + (i * (1/(ns+1))*(schiff.corners[2] - schiff.corners[3]))

			vektor = Physics.F_R(ns, schiff.cw_s, schiff.l, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, True))
			F_res = F_res + vektor

		# Backbordseite
		for i in range(1,ns+1):
			p = schiff.corners[0] + (i * (1/(ns+1))*(schiff.corners[1] - schiff.corners[0]))

			vektor = Physics.F_R(ns, schiff.cw_s, schiff.l, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, True))
			F_res = F_res + vektor

		return F_res

	def M_ges(schiff,stroemung, nf, ns):
		M_res = 0

		# Bug
		for i in range(1,nf+1):
			p = schiff.corners[1] + (i * (1/(nf+1))*(schiff.corners[2] - schiff.corners[1]))

			skalar = Physics.M_i(Physics.F_R(nf, schiff.cw_f, schiff.b, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, False)), p - schiff.getCenter())
			M_res = M_res + skalar

		# Heck
		for i in range(1,nf+1):
			p = schiff.corners[0] + (i * (1/(nf+1))*(schiff.corners[3] - schiff.corners[0]))

			skalar = Physics.M_i(Physics.F_R(nf, schiff.cw_f, schiff.b, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, False)), p - schiff.getCenter())
			M_res = M_res + skalar

		# Steuerbordseite
		for i in range(1,ns+1):
			p = schiff.corners[3] + (i * (1/(ns+1))*(schiff.corners[2] - schiff.corners[3]))

			skalar = Physics.M_i(Physics.F_R(ns, schiff.cw_s, schiff.l, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, True)), p - schiff.getCenter())
			M_res = M_res + skalar

		# Backbordseite
		for i in range(1,ns+1):
			p = schiff.corners[0] + (i * (1/(ns+1))*(schiff.corners[1] - schiff.corners[0]))

			skalar = Physics.M_i(Physics.F_R(ns, schiff.cw_s, schiff.l, schiff.h, schiff.velocity, Physics.closestVector(p, stroemung), Physics.makeOberflvek(schiff, True)), p - schiff.getCenter())
			M_res = M_res + skalar

		return M_res

# GUI / Animation
fig, ax = plt.subplots()
ax.set_xlim(-500, 900)
ax.set_ylim(-500, 900)
ax.set_title("Lastkahn Simulation")

# Scaling factors für Strömungsdaten
k = 150/2 # 2 Einheiten aus Datei -> 150m im Modell
k_v = 18 # damit maximale Fließgeschwindigkeit ca. 3 m/s ist

# Einlesen der Strömungsdaten
stroemung = Stroemung()

with open(sys.argv[2]) as inputfile:
	state = -1 # 0 => Begrenzung links, 1 => Begrenzung rechts, 2 => Vektoren
	for line in inputfile:
		if line == "\n":
			state = -1
			continue
		if line == "Begrenzung links\n":
			state = 0
			continue
		if line == "Begrenzung rechts\n":
			state = 1
			continue
		if line == "Vektoren\n":
			state = 2
			continue

		if state == 0:
			stroemung.addBegrenzungsPunkt(True, list(float(x) * k for x in line.split()))

		if state == 1:
			stroemung.addBegrenzungsPunkt(False, list(float(x) * k for x in line.split()))

		if state == 2:
			vektor = list(float(x) for x in line.split())
			vektor[0] = vektor[0] * k
			vektor[1] = vektor[1] * k
			vektor[2] = vektor[2] * k_v
			vektor[3] = vektor[3] * k_v
			stroemung.addVektor(vektor)

	stroemung.finalizeBegrenzungen(ax)


# Einlesen der Schiffsparameter
params = {}

with open(sys.argv[1]) as inputfile:
	params = json.load(inputfile)
params['m'] = Physics.calcM(params['m_max'], params['load'])
params['h'] = Physics.calcH(params['h_max'],params['m_max'],params['m'],params['l'],params['b'])
params['I'] = Physics.I(params['m'], params['l'], params['b'])
# Schiff zentriert in Strömung platzieren
params['c0'] = np.array([750-params['b']/2, -300])
params['c2'] = np.array([750+params['b']/2, -300+params['l']])
params['a'] = 0
params['a_max'] = Physics.a_max(params['v_max'], params['h'], params['b'], params['m'])

# Schiff-Objekt erstellen
schiff = Ship(params['c0'], params['c2'], params['a'], params['l'], params['b'], params['h'], params['m'], params['I'], params['cw_f'], params['cw_s'], params['a_max'], params['v_max'], params['w_maxRudder'])

# Plotting Schiff
codes = np.array([Path.MOVETO,
		Path.LINETO,
		Path.LINETO,
		Path.LINETO,
		Path.CLOSEPOLY,
		])

schiffPath = Path(schiff.corners, codes)
schiffPatch = PathPatch(schiffPath, fc='red')
ax.add_patch(schiffPatch)

# Plotting controls / control surfaces
schiffGrossPatch = patches.Rectangle((-15, 0), 30, 200, fc='r', ec='black')
ax.add_patch(schiffGrossPatch)

rudderPatch = patches.Rectangle((-5,-100), 10, 100, fc='g', ec='black')
ax.add_patch(rudderPatch)

schubText = ax.text(-100, -200, "Schub: {}%".format(int(round(schiff.thrust*100, 0))))

# Animation
def animate(i):
	schiff.update(stroemung)

	t_rudder = transforms.Affine2D().rotate(-schiff.rudder*np.pi/4) + ax.transData
	rudderPatch.set_transform(t_rudder)

	schubText.set_text("Schub: {}%".format(int(round(schiff.thrust*100, 0))))

	return [schiffPatch, rudderPatch, schubText, ]

ani = FuncAnimation(fig, animate, interval=100, blit=True)

# Plotting Strömung
plt.quiver(stroemung.x, stroemung.y, stroemung.u, stroemung.v, color=(0,0,1,0.3))

# Plotting Begrenzungen
begrenzungCodesLinks = np.ones(len(stroemung.begrenzungLinks[:-1]), int) * Path.LINETO
begrenzungCodesLinks[0] = Path.MOVETO

begrenzungCodesRechts = np.ones(len(stroemung.begrenzungRechts[:-1]), int) * Path.LINETO
begrenzungCodesRechts[0] = Path.MOVETO

begrenzungLinksPath = Path(stroemung.begrenzungLinks[:-1], begrenzungCodesLinks)
begrenzungLinksPatch = PathPatch(begrenzungLinksPath, lw=4, ec="r", fill=False)
ax.add_patch(begrenzungLinksPatch)

begrenzungRechtsPath = Path(stroemung.begrenzungRechts[:-1], begrenzungCodesRechts)
begrenzungRechtsPatch = PathPatch(begrenzungRechtsPath, lw=4, ec="g", fill=False)
ax.add_patch(begrenzungRechtsPatch)


# Polygone für Kollisionserkennung
begrenzungLinksPoly = Polygon(stroemung.begrenzungLinks)
begrenzungRechtsPoly = Polygon(stroemung.begrenzungRechts)
zielPoly = Polygon([stroemung.begrenzungLinks[-2], stroemung.begrenzungRechts[-2], [ax.get_xlim()[1], ax.get_ylim()[1]], [ax.get_xlim()[0], ax.get_ylim()[1]], [ax.get_xlim()[0], ax.get_ylim()[1]], [ax.get_xlim()[0], ax.get_ylim()[0]]])

# show Plot
plt.axis('equal')
plt.gca().set_aspect('equal', adjustable='box')
plt.show(block=False)

# PyGame init for keyboard inputs
pygame.init()
pygame.key.set_repeat(1,25)

# loop
# Von Anfang an pausieren
paused = True
ani.event_source.stop()
while True:

	# Check collision
	for point in schiff.getCornerPoints():
		if begrenzungLinksPoly.contains(point) or begrenzungRechtsPoly.contains(point) or zielPoly.contains(point):
			# Simulation pausieren
			ani.event_source.stop()
			paused = True

	# Keyboard inputs
	for event in pygame.event.get():
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				pygame.quit()
			if event.key == pygame.K_LEFT:
				schiff.steerLeft()
			if event.key == pygame.K_RIGHT:
				schiff.steerRight()
			if event.key == pygame.K_UP:
				schiff.increaseThrust()
			if event.key == pygame.K_DOWN:
				schiff.decreaseThrust()
			if event.key == pygame.K_SPACE:
				# Space bar to pause / unpause
				if paused:
					ani.event_source.start()
					paused = False
					time.sleep(0.5)
				else:
					ani.event_source.stop()
					paused = True
					time.sleep(0.5)
